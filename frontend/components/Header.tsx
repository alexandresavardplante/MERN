import { ReactNode, useState } from "react";
import Link from "next/link";
import styles from "../styles/header.module.scss";
import { AiOutlineMenu, AiOutlineClose } from "react-icons/ai";
import { FaBeer } from "react-icons/fa";
import cn from "classnames";

const Header = (): ReactNode => {
  const [showMobileMenu, setShowMobileMenu] = useState<boolean>(false);

  return (
    <nav className={styles.navBar}>
      <FaBeer aria-label="Logo du site" className={styles.logo} />

      <div
        className={cn({
          [styles.navBarLinks]: true,
          [styles.active]: showMobileMenu,
        })}
      >
        <ul>
          <li>
            <Link href="/breuvages">
              <a>Breuvages</a>
            </Link>
          </li>
          <li>
            <Link href="/contact">
              <a>Nous contacter</a>
            </Link>
          </li>
          <li>
            <Link href="/apropos">
              <a>À propos</a>
            </Link>
          </li>
        </ul>
      </div>

      {!showMobileMenu ? (
        <AiOutlineMenu
          onClick={() => setShowMobileMenu(!showMobileMenu)}
          className={styles.menu}
        />
      ) : (
        <AiOutlineClose
          onClick={() => setShowMobileMenu(!showMobileMenu)}
          className={styles.menu}
        />
      )}
    </nav>
  );
};

export default Header;
